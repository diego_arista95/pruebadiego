<?php

class plantillaMVC {

    public function plantilla() {
    include "views/template.php";
    }


    public function enlacesPaginasController() {
        
        if (isset($_GET["action"])) {
            $enlacesController = $_GET["action"];
        }
    
        else {
            $enlacesController = "index";
        }
   
    $respuesta = EnlacesPaginas::enlacesPaginasModel($enlacesController);
    
    include $respuesta;

    
    }


    public function registroUsuarioController(){
        if (isset($_POST["username_register"])) {
        #preg_match = Realiza una comparación con una expresión regular TOMADO DE STACK
                        if (preg_match('/^[a-zA-Z0-9]+$/', $_POST["username_register"])  &&
                            preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["email_register"]) &&
                            preg_match('/^[a-zA-Z0-9]+$/', $_POST["password_register"])) {
                        
                        

                            #crypt() devolverá el hash de un string utilizando el algoritmo estándar basado en DES de Unix o algoritmos alternativos que puedan estar disponibles en el sistema.

			   	$encriptar = crypt($_POST["password_register"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                                $datosController  = array("username"=>$_POST["username_register"],
                                                            "email"=>$_POST["email_register"],
                                                            "password"=>$encriptar);
                                    $respuesta = Datos::registroUsuarioModel($datosController, "users"); 
                                    // echo $respuesta;
                                    if ($respuesta == "success") {
                                        
                                        // $archivoActual = $_SERVER['PHP_SELF'];
                                        header( "location:ok" ); 
                                    
                                    
                                    } else {
                                        header("location:index.php");
                                    }
                            }
                }
             
            }
//actualizar contra
            public function cambioContraController(){
                if (isset($_POST["email"])) {

                    	#preg_match = Realiza una comparación con una expresión regular
                    if (
                        preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["email"]) &&
                        preg_match('/^[a-zA-Z0-9]+$/', $_POST["password"])) {



                            #crypt() devolverá el hash de un string utilizando el algoritmo estándar basado en DES de Unix o algoritmos alternativos que puedan estar disponibles en el sistema.

			            	$encriptar = crypt($_POST["password"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                    
                            $datosController  = array(
                                                        "email"=>$_POST["email"],
                                                        "password"=>$encriptar);
                                $respuesta = Datos::cambioContraModel($datosController, "users"); 
                                // echo $respuesta;
                                if ($respuesta == "success") {
                                    
                                    // $archivoActual = $_SERVER['PHP_SELF'];
                                    header( "location:cambiook" ); 
                                
                                   
                                } else {
                                    echo "error";
                                }
                        }
                    }
             }















// LOGIN DE USUARIOS
//CODIGO DE UN CURSO QUE TOME
            public function ingresoUsuarioController(){
                if (isset($_POST["email_login"])) {
                    if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["email_login"]) &&
                        preg_match('/^[a-zA-Z0-9]+$/', $_POST["password_login"])) {


                            #crypt() devolverá el hash de un string utilizando el algoritmo estándar basado en DES de Unix o algoritmos alternativos que puedan estar disponibles en el sistema.
			            	$encriptar = crypt($_POST["password_login"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                    
                            $datosController  = array("email"=>$_POST["email_login"],
                                                        "password"=>$encriptar);
                                $respuesta = Datos::ingresoUsuarioModel($datosController, "users"); 
                                // var_dump($respuesta);

                                $usuario = $_POST["email_login"];
                                $intentos = $respuesta["intents"];
                                $maxIntentos = 2;
                                if ($intentos < $maxIntentos) {
                               

                                if ($respuesta["email"] == $_POST["email_login"] && $respuesta["password"] == $encriptar) {
                                    session_start();
                                    $_SESSION["validar"] = true;
                                    $intentos = 0;
                                    $datosController = array("UsuarioActual" => $usuario, "ActualizarIntentos" => $intentos);
                                    $respuestaActualizarIntentos = Datos::intentosUsuarioModel($datosController, "users");
                                    header( "location:inicio" ); 
                                
                                 } else {
                                     ++$intentos;
                                    $datosController = array("UsuarioActual" => $usuario, "ActualizarIntentos" => $intentos);
                                    $respuestaActualizarIntentos = Datos::intentosUsuarioModel($datosController, "users");
                                    header("location:fallo");
                                }
                            }
                            else{
                                    $intentos = 0;
                                    $datosController = array("UsuarioActual" => $usuario, "ActualizarIntentos" => $intentos);
                                    $respuestaActualizarIntentos = Datos::intentosUsuarioModel($datosController, "users");
                                    header("location:fallo3intentos");


                            }
                         }
                            
                    
                    }
            }
  



                    //  VERIFICAR USUARIOS EXISTENTES

             public function validarUsuarioController($validarUsuario){
                $datosController = $validarUsuario;
                $respuesta = Datos::validarUsuarioModel($datosController, "users");
            
                if(count($respuesta) > 0) {
                    echo 0;
                } 
                else{
                    echo 1;
                }

             }


              //  VERIFICAR EMAIL EXISTENTES

              public function validarEmailController($validarEmail){
                $datosController = $validarEmail;
                $respuesta = Datos::validarEmailModel($datosController, "users");
            
                if(count($respuesta) > 0) {
                    echo 0;
                } 
                else{
                    echo 1;
                }
            }
}
