<?php

ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diego Test</title>
    <link rel="stylesheet" href="views/resources/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="views/resources/css/style.css">
</head>
<body>
   
<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="inicio">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Relleno
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Ejemplo</a>
                    </div>
                </li>    
                </ul>
                <ul class="pull-right navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="ingreso">Entrar</a>
                

                    </li>  <li class="nav-item">
                    <a class="nav-link" href="registro">Registrarse</a>
                

                    </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Usuario
                    </a>
                 
                    
                    
              
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="salir">Cerrar Sesión</a>
                   
                    </div>
                </li>  
                
                </ul>
            
            </div>
        </div>
    </nav>
</header>

<main class="d-flex justify-content-center align-items-center text-center">
    <div class="container">
  
<?php 
    $mvc = new plantillaMVC();

    $mvc -> enlacesPaginasController();
?>

    </div>


</main>
<footer class="text-center">
    <div class="col-md-12 ">
    <p>Derechos reservados 2020 by Diego</p>
    
    </div>
    
</footer>
    <script src="views/resources/js/jquery.min.js"></script>
    <script src="views/resources/js/bootstrap.min.js"></script>
    <script src="views/js/validarRegistro.js"></script>
    <script src="views/js/validarIngreso.js"></script>
    <script src="views/js/validarCambio.js"></script>
</body>
</html>
<?php
//nos deja recargar la misma pagina
ob_end_flush();
?>