




<div class="d-flex  align-items-center  justify-content-center col-12">

        <div class="mt-4 mb-5 col-md-5">
            <div class="col-12 text-center ">
                <h2 class="p-2">Cambio de contraseña</h2>
            </div>
            <div class=" bg-light p-3 ">
            
                <form method="post" onsubmit="return validarRegistro()">
                  
                    <div class="form-group">
                        <label for="email_register">Correo <span></span></label>
                        <input class="form-control" type="email" name="email" id="email_register" placeholder="Escriba correctamente su correo" required>
                    </div>

                    <div class="form-group">
                        <label for="password_register">Nueva Contraseña</label>
                        <input class="form-control" type="password" name="password" id="password_register" placeholder="Mínimo 6 caracteres, incluir números y Mayúsculas" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>
                    </div>

                    <div class="form-group col-12">
                       
                        <button class="btn btn-primary form-control" type="submit">Enviar</button>
                    </div>
    <?php 
  
  // creamos un objeto lalmado registro con la clase mvccontroller

  $cambio = new  plantillaMVC();
  
  $cambio->cambioContraController();


  if (isset($_GET["action"])) {
      if ($_GET["action"] == "cambiook") {
         echo "<div class='alert-success text-center p-1'>Contraseña actualizada con éxito <a href='index.php?action=ingreso'>Ingrese</a></div> ";
      }
  
  }
?>
     </form>
            </div>
  </div>
    </div>


