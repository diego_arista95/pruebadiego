




<div class="d-flex  align-items-center  justify-content-center col-12">

        <div class="mt-4 mb-5 col-md-5">
            <div class="col-12 text-center ">
                <h2 class="p-2">Registro</h2>
            </div>
            <div class=" bg-light p-3 ">
            
                <form method="post" onsubmit="return validarRegistro()">
                    <div class="form-group">
                        <label for="username_register">Nombre de usuario <span></span></label>
                        <input class="form-control" type="text" name="username_register" id="username_register" maxlength="10" placeholder="Máximo 10 caracteres" required>
                    </div>
                    <div class="form-group">
                        <label for="email_register">Correo <span></span></label>
                        <input class="form-control" type="email" name="email_register" id="email_register" placeholder="Escriba correctamente su correo" required>
                    </div>

                    <div class="form-group">
                        <label for="password_register">Contraseña</label>
                        <input class="form-control" type="password" name="password_register" id="password_register" placeholder="Mínimo 6 caracteres, incluir números y Mayúsculas" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>
                    </div>

                    <div class="form-group col-12">
                        <label for="terminos"></label>
                        <p class="text-center"> <input  type="checkbox" name="terminos" id="terminos"> <a href="#">Acepta Términos y condiciones</a></p>
                        <button class="btn btn-primary form-control" type="submit">Crear cuenta</button>
                    </div>
    <?php 
  
  // creamos un objeto lalmado registro con la clase mvccontroller

  $registro = new  plantillaMVC();
  
  $registro -> registroUsuarioController();


  if (isset($_GET["action"])) {
      if ($_GET["action"] == "ok") {
         echo "<div class='alert-success text-center p-1'>Registro exitoso <a href='index.php?action=ingreso'>Ingrese</a></div> ";
      }
  
  }
?>
     </form>
            </div>
  </div>
    </div>


