/*************VALIDAR REGISTRO*************** */


var usuarioExistente = false;
var emailExistente = false;

$("#username_register").change(function(){
    var usuario = $("#username_register").val();
    var datos = new FormData();
    datos.append("validarUsuario", usuario);
    $.ajax({
        url:"views/modules/ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            if(respuesta == 0){
               
                $("label[for='username_register'] span").html('<div class="alert-danger text-center p-2 ">Este Usuario ya existe, intente con otro.</div>');
                usuarioExistente = true;
            } else{
                $("label[for='username_register'] span").html("");
                usuarioExistente = false;
            }


          //  console.log('respuesta de PHP:'+ usuario);

           
        }
    });
  



/*************FIN VALIDAR REGISTRO*********** */




/*************VALIDAR EMAIL*************** */

$("#email_register").change(function(){
    var email = $("#email_register").val();
    var datos = new FormData();
    datos.append("validarEmail", email);
    $.ajax({
        url:"views/modules/ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            if(respuesta == 0){
               
                $("label[for='email_register'] span").html('<div class="alert-danger text-center p-2 ">Este correo ya existe, intente con otro.</div>');
                emailExistente = true;
            } else{
                $("label[for='email_register'] span").html("");
                emailExistente = false;
            }


           // console.log('respuesta de PHP:'+ email);

           
        }
    });
  


});




})
/*************FIN VALIDAR REGISTRO*********** */

/*************VALIDAR REGISTRO*************** */
function validarRegistro(){
    var usuario = document.querySelector("#username_register").value;
    var password = document.querySelector("#password_register").value;
    var email = document.querySelector("#email_register").value;
    var terminos = document.querySelector("#terminos").checked;
    //VALIDAR USUARIO
    if (usuario !="") {
        //creamos variables locales
        //contamos los carateres con .lenght
        //alt 94 ^
        var carecteres = usuario.length;
        //expresion regular que tenga de la a a la z en minuscula y mayusculas del 0 al 9 y sin caracteres especiales
        var expresion = /^[a-zA-Z0-9]*$/;
        
        if (carecteres > 10) {
            document.querySelector("label[for='username_register']").innerHTML+="<br>  <div class='alert-danger text-center p-1'>Escriba menos de 10 caracteres.</div>";
            return false;
        }
        // si la expresion regular es diferente a lo que se pide del input, el test que le hace, es diferente a lo de la expresion regular
        //es por que hay caracteres especiales
        if (!expresion.test(usuario)) {
            document.querySelector("label[for='username_register']").innerHTML+="<br>  <div class='alert-danger text-center p-1'>No escriba caracteres especiales.</div>";
            return false;
        }

        if (usuarioExistente) {
            document.querySelector("label[for='username_register'] span").innerHTML+=" <div class='alert-danger text-center p-1 m-1'>Este usuario ya existe.</div>";
            return false;
        }


       



    }















//VALIDAR PASSWORD

    if (password !="") {
        //creamos variables locales
        //contamos los carateres con .lenght
        //alt 94 ^
        var carecteres = password.length;
        //expresion regular que tenga de la a a la z en minuscula y mayusculas del 0 al 9 y sin caracteres especiales
        var expresion = /^[a-zA-Z0-9]*$/;
        
        if (carecteres < 6) {
            document.querySelector("label[for='password_register']").innerHTML+="<br> <div class='alert-danger text-center p-1'> Escriba más de 6 caracteres.</div>";
            return false;
        }
        // si la expresion regular es diferente a lo que se pide del input, el test que le hace, es diferente a lo de la expresion regular
        //es por que hay caracteres especiales
        if (!expresion.test(password)) {
            document.querySelector("label[for='password_register']").innerHTML+="<br>  <div class='alert-danger text-center p-1'>No escriba caracteres especiales.</div>";
            return false;
        }
    }

    //VALIDAR EMAIL
    
    if (email !="") {
        //creamos variables locales
        //alt 94 ^
      
        var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
        
        // si la expresion regular es diferente a lo que se pide del input, el test que le hace, es diferente a lo de la expresion regular
        //es por que hay caracteres especiales
        if (!expresion.test(email)) {
            document.querySelector("label[for='email_register']").innerHTML+="<br>  <div class='alert-danger text-center p-1'>Escriba correctamente su correo.</div>";
            return false;
        }

        if (emailExistente) {
            document.querySelector("label[for='email_register'] span").innerHTML+=" <div class='alert-danger text-center p-1 m-1'>Este correo ya existe.</div>";
            return false;
        }
    }

//VALIDAR TERMINOS


        if (!terminos) {
            document.querySelector("label[for='terminos']").innerHTML+="<br> <div class='alert-danger text-center p-1'>No se puede registrar, ¡Acepte términos y condiciones!</div>";
            //tomamos los valores que ya se introdujeron para que no se borren
            document.querySelector("#username_register").value = usuario;	
            document.querySelector("#password_register").value = password;	
            document.querySelector("#email_register").value = email;
            return false;
        }




    return true;

}

/*************FIN VALIDAR REGISTRO*********** */