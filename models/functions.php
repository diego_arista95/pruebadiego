<?php
require_once "conexion.php";

class Datos extends Conexion{

    // REGISTRO DE USUARIOS
    public function registroUsuarioModel($datosModel, $tabla) {
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(username, email, password) 
        VALUES (:username, :email, :pass)");

        $stmt->bindParam(":username", $datosModel["username"], PDO::PARAM_STR);
        $stmt->bindParam(":email", $datosModel["email"], PDO::PARAM_STR);
        $stmt->bindParam(":pass", $datosModel["password"], PDO::PARAM_STR);
        

        if ($stmt->execute()) {
            return "success";
        } else {
            return "error";
        }
        $stmt->close();

    }




   // CAMBIO DE CONTRASEÑA DE USUARIOS
   public function cambioContraModel($datosModel, $tabla) {
    $stmt = Conexion::conectar()->prepare("UPDATE $tabla 
    SET  password = :pass WHERE email = :email");


    $stmt->bindParam(":email", $datosModel["email"], PDO::PARAM_STR);
    $stmt->bindParam(":pass", $datosModel["password"], PDO::PARAM_STR);
    

    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }
    $stmt->close();

}




// INGRESO DE USUARIOS

public function ingresoUsuarioModel($datosModel, $tabla) {
    $stmt = Conexion::conectar()->prepare("SELECT email, password, intents FROM $tabla WHERE email = :email");
    $stmt->bindParam(":email", $datosModel["email"], PDO::PARAM_STR);
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();

}




// INTENTOS DE USUARIOS
public function intentosUsuarioModel($datosModel, $tabla) {
    $stmt = Conexion::conectar()->prepare("UPDATE $tabla 
    SET  intents = :intentos WHERE email = :email");
      $stmt->bindParam(":email", $datosModel["UsuarioActual"], PDO::PARAM_STR);
      $stmt->bindParam(":intentos", $datosModel["ActualizarIntentos"], PDO::PARAM_STR);
      

    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }
    $stmt->close();
    


}






        // VALIDAR USUARIOS EXISTENTES
        public function validarUsuarioModel($datosModel, $tabla) {
            $stmt = Conexion::conectar()->prepare("SELECT  username FROM $tabla WHERE username = :usuario");
            $stmt->bindParam(":usuario", $datosModel, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
            $stmt->close(); 

        }


  // VALIDAR USUARIOS EXISTENTES
    public function validarEmailModel($datosModel, $tabla) {
        $stmt = Conexion::conectar()->prepare("SELECT email FROM $tabla WHERE email = :email");
        $stmt->bindParam(":email", $datosModel, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
        $stmt->close(); 

    }


}
// bindParam() vincula uan variable php aun parametro de sustituciocon un nombre o signo de interrogacion
// de sentencias sql
// :: operadores de ambito que se usa para heredar una clase
// si solo se ejecuta no se necesita heredar
// si vamso a manipular uan clase padre se hereda y se necesita mandar lalmar el archivo donde esat esa clase padre
 